//
//  AddressListModel.m
//  DEMO_AddressList
//
//  Created by 熊章哲 on 2018/7/15.
//  Copyright © 2018年 ElevenSx. All rights reserved.
//

#import "AddressListModel.h"
#import "MJExtension.h"

@implementation AddressListModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"name" : @"firstLetter",
             @"tagArr" : @"listLabels",
             };
}

@end


@implementation TagModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{};
}

@end
