//
//  AddressListModel.h
//  DEMO_AddressList
//
//  Created by 熊章哲 on 2018/7/15.
//  Copyright © 2018年 ElevenSx. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TagModel;
@interface AddressListModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, strong) NSMutableArray<TagModel *> *tagArr;

@end


@interface TagModel : NSObject

@property (nonatomic, copy) NSString *tagName;
@property (nonatomic, assign) BOOL isSelected;

@end
