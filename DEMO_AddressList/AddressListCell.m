//
//  AddressListCell.m
//  DEMO_AddressList
//
//  Created by 熊章哲 on 2018/7/15.
//  Copyright © 2018年 ElevenSx. All rights reserved.
//

#define kUIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#import "AddressListCell.h"
#import "AddressListModel.h"

@interface AddressListCell ()

@property (weak, nonatomic) IBOutlet UILabel *lb_name;
@property (weak, nonatomic) IBOutlet UIButton *btn_flag;

@end


@implementation AddressListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentView.backgroundColor = kUIColorFromRGB(0xFFFFE0);
}

- (void)loadAddressListCellWithModel:(TagModel *)model {
    self.lb_name.text = model.tagName;
    self.btn_flag.selected = model.isSelected;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
