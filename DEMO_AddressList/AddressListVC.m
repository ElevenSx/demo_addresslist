//
//  AddressListVC.m
//  DEMO_AddressList
//
//  Created by 熊章哲 on 2018/7/15.
//  Copyright © 2018年 ElevenSx. All rights reserved.
//

#define SCREEN_WIDTH            [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT           [UIScreen mainScreen].bounds.size.height
#define kHeightCoefficient      (SCREEN_HEIGHT == 812.0 ? 667.0/667.0 : SCREEN_HEIGHT/667.0)
#define kSafeAreaTopHeight      (SCREEN_HEIGHT == 812.0 ? 88 : 64)
#define kSAH                    (SCREEN_HEIGHT == 812.0 ? 24 : 0)
#define kSafeAreaBottomHeight   (SCREEN_HEIGHT == 812.0 ? 34 : 0)
#define kSafeAreaContentH       (SCREEN_HEIGHT - (kSafeAreaTopHeight + kSafeAreaBottomHeight + 49.f))
#define kNavMaxY                64.f
#define kMargin                 15.f

#define kUIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]



#import "AddressListVC.h"
#import "AddressListCell.h"
#import "AddressListModel.h"

#import "MJExtension.h"

@interface AddressListVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tb;
@property (nonatomic, strong) NSDictionary *dic;
@property (nonatomic, strong) NSMutableArray *modelArr;

@end

@implementation AddressListVC

- (instancetype)init {
    if (self = [super init]) {
        _modelArr = [NSMutableArray arrayWithCapacity:0];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setupData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupUI {
    self.view.backgroundColor = kUIColorFromRGB(0xFFF5EE);
    [self.view addSubview:self.tb];
}

- (void)setupData {
    [self.modelArr removeAllObjects];
    
    for (NSDictionary *dic in self.dic[@"data"]) {
        AddressListModel *model = [AddressListModel mj_objectWithKeyValues:dic];
        
        NSMutableArray *tagsArr = [NSMutableArray arrayWithCapacity:0];
        for (NSString *str in model.tagArr) {
            TagModel *model = [[TagModel alloc] init];
            model.tagName = str;
            model.isSelected = NO;

            [tagsArr addObject:model];
        }
        
        model.tagArr = tagsArr;
        
        [self.modelArr addObject:model];
    }
    
    [self.tb reloadData];
}


#pragma make - Delegate
#pragma make UITableViewDelegate Methods
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AddressListCell class])];
    
    AddressListModel *listModel = self.modelArr[indexPath.section];
    TagModel *model = [self.modelArr[indexPath.section] tagArr][indexPath.row];
    
    [cell loadAddressListCellWithModel:model];
    
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.modelArr[section] tagArr].count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.modelArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *v_header = ({
        v_header = [[UIView alloc] init];
        v_header.frame = CGRectMake(0, 0, SCREEN_WIDTH, 30.f);
        v_header.backgroundColor = kUIColorFromRGB(0xFFE1FF);
        
        v_header;
    });
    
    CGFloat btnW = 20.f;
    CGFloat viewH = v_header.bounds.size.height;
    
    UILabel *lb_name = ({
        lb_name = [[UILabel alloc] init];
        lb_name.frame = CGRectMake(kMargin, 0, v_header.bounds.size.width - kMargin * 3 - btnW, viewH);
        lb_name.text = [self.modelArr[section] name];
        [v_header addSubview:lb_name];
        
        lb_name;
    });
    
    UIButton *btn_state = ({
        btn_state = [[UIButton alloc] init];
        btn_state.frame = CGRectMake(CGRectGetMaxX(lb_name.frame) + kMargin, 0, btnW, viewH);
        [btn_state setImage:[UIImage imageNamed:@"addressList_unchecked"] forState:UIControlStateNormal];
        [btn_state setImage:[UIImage imageNamed:@"addressList_select"] forState:UIControlStateSelected];
        [v_header addSubview:btn_state];
        
        btn_state;
    });
    
    return v_header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TagModel *model = [self.modelArr[indexPath.section] tagArr][indexPath.row];
    model.isSelected = !model.isSelected;
    
    NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:indexPath.section];
    [tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma make - Lazy
- (UITableView *)tb {
    if (!_tb) {
        _tb = [[UITableView alloc] init];
        _tb.frame = CGRectMake(0, kSAH + kNavMaxY, SCREEN_WIDTH, SCREEN_HEIGHT - (kSAH + kNavMaxY));
        _tb.delegate = self;
        _tb.dataSource = self;
        _tb.backgroundColor = [UIColor clearColor];
        [_tb registerNib:[UINib nibWithNibName:NSStringFromClass([AddressListCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([AddressListCell class])];
    }
    
    return _tb;
}


#pragma mark 假数据
- (NSDictionary *)dic {
    if (!_dic) {
        _dic = @{
                 @"success": @1,
                 @"code": @1,
                 @"message": @"操作成功",
                 @"data": @[
                         @{
                             @"listLabels": @[
                                     @"标签1",
                                     @"标签2",
                                     @"标签3"
                                     ],
                             @"firstLetter": @"B"
                             },
                         @{
                             @"listLabels": @[
                                     @"测试1",
                                     @"测试2",
                                     @"测试3",
                                     @"测试4",
                                     @"测试标签4",
                                     @"测试测底1",
                                     @"测试测底2",
                                     @"测试测底3",
                                     @"测试测底4",
                                     @"ceshi"
                                     ],
                             @"firstLetter": @"C"
                             },
                         @{
                             @"listLabels": @[
                                     @"十",
                                     @"十哦",
                                     @"sh"
                                     ],
                             @"firstLetter": @"S"
                             },
                         @{
                             @"listLabels": @[
                                     @"低的跟家里"
                                     ],
                             @"firstLetter": @"D"
                             },
                         @{
                             @"listLabels": @[
                                     @"jjdfjg"
                                     ],
                             @"firstLetter": @"J"
                             },
                         @{
                             @"listLabels": @[
                                     @"labe2",
                                     @"label"
                                     ],
                             @"firstLetter": @"L"
                             },
                         @{
                             @"listLabels": @[
                                     @"膜拜"
                                     ],
                             @"firstLetter": @"M"
                             },
                         @{
                             @"listLabels": @[
                                     @"note",
                                     @"note1"
                                     ],
                             @"firstLetter": @"N"
                             },
                         @{
                             @"listLabels": @[
                                     @"ofo",
                                     @"123",
                                     @"2biaoqian"
                                     ],
                             @"firstLetter": @"O"
                             }
                         ],
                 @"roleName": @"nil"
                 };
    }
    
    return _dic;
}


@end
