//
//  ViewController.m
//  DEMO_AddressList
//
//  Created by 熊章哲 on 2018/7/15.
//  Copyright © 2018年 ElevenSx. All rights reserved.
//

#import "ViewController.h"
#import "AddressListVC.h"

@interface ViewController ()

@property (nonatomic, strong) UIButton *btn_addressList;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.btn_addressList];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)jumpToAddressList:(UIButton *)sender {
    AddressListVC *vc = [[AddressListVC alloc] init];
    [self presentViewController:vc animated:YES completion:nil];
}

- (UIButton *)btn_addressList {
    if (!_btn_addressList) {
        _btn_addressList = [[UIButton alloc] init];
        _btn_addressList.frame = CGRectMake(0, 0, 100.f, 30.f);
        _btn_addressList.center = self.view.center;
        _btn_addressList.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btn_addressList setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_btn_addressList setTitle:@"通讯录" forState:UIControlStateNormal];
        [_btn_addressList addTarget:self action:@selector(jumpToAddressList:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _btn_addressList;
}

@end
