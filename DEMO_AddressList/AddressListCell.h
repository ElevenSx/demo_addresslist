//
//  AddressListCell.h
//  DEMO_AddressList
//
//  Created by 熊章哲 on 2018/7/15.
//  Copyright © 2018年 ElevenSx. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TagModel;
@interface AddressListCell : UITableViewCell

- (void)loadAddressListCellWithModel:(TagModel *)model;

@end
